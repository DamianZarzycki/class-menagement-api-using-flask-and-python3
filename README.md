**Backend code written in Python 3 using Flask framework for class menagement module in Angular project for TAU classes.**

1. Clone project
1. Go to https://console.firebase.google.com/project/**YOUR_PROJECT**/settings/serviceaccounts/adminsdk and generate new private key and it will download .json file (You must have been added to the project)
1. Open console, paste **set GOOGLE_APLICATION_CREDENTIALS='path to .json file with genereted keys'** or set it manualy by creating **GOOGLE_APLICATION_CREDENTIALS** variable in system variable and provide path to the same .json file
1. Open console in root folder of project You have cloned.
1. Paste **python -m venv venv** and then **venv/scripts/activate**
1. Paste **cd '.\Class menagement API\'**
1. Run **pip install -r requirements.txt**
1. Paste **cd app**
1. Run **python main.py** and You are ready to go
1. For test run **pytest**
