from service.class_repository import ClassRepository


class ClassLogic:
    def delete_class(self, class_to_delete_id):
        status = ClassRepository().delete_class(class_to_delete_id)
        return status

    def add_class(self, class_to_add):
        status = ClassRepository().add_class(class_to_add)
        return status

    def get_class(self, class_to_get_id):
        status = ClassRepository().get_class(class_to_get_id)
        return status

    def get_classes(self):
        status = ClassRepository().get_classes()
        return status
