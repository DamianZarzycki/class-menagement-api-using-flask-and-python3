from service.exercise_repository import ExerciseRepository


class ExerciseBusinessLogic:
    def add_exercise_for_student(self, body):
        status = ExerciseRepository().add_exercise_for_student(body)
        return status

    def add_exercise_for_class(self, body):
        status = ExerciseRepository().add_exercise_to_class(body)
        return status

    def get_exercise_from_class(self, class_id):
        status = ExerciseRepository().get_exercise_from_class(class_id)
        return status
