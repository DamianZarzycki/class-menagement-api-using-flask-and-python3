from service.student_repository import StudentRepository


class StudentLogic:
    def delete_student_from_class(self, student_id, class_id):
        status = StudentRepository().delete_student_from_class(student_id, class_id)
        return status

    def add_student_to_class(self, student_to_add):
        status = StudentRepository().add_class(student_to_add)
        return status

    def get_specyfic_student_from_class(self, student_id, class_id):
        status = StudentRepository().get_specyfic_student_from_class(
            student_id, class_id
        )
        return status

    def get_students_from_class(self, class_id):
        status = StudentRepository().get_students_from_class(class_id)
        return status

    def update_student(self, data_to_update):
        status = StudentRepository().update_student(data_to_update)
        return status
