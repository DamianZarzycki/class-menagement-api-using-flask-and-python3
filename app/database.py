from google.cloud import firestore

db = firestore.Client()


def collection(collection):
    """
    :param body: string
    :return: firebase ref
    """
    return db.collection(collection)
