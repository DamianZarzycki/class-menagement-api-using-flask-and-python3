import pytest


@pytest.fixture(scope="function")
def correct_class_id_and_student_id_combination():
    body = {"class_id": "05lKi6RjKhO6b0XjKMyD", "student_id": "wRdGEWdLK0Y5Guxs9k9N"}
    return body


@pytest.fixture(scope="function")
def correct_class_id_and_student_id_combination_to_delete():
    body = {"class_id": "05lKi6RjKhO6b0XjKMyD", "student_id": "wRdGEWdLK0Y5Guxs9k9N"}
    return body


@pytest.fixture(scope="function")
def correct_class_request_body():
    body = {
        "class_name": "Valid",
        "class_semester": "Letni",
        "class_year": 1997,
        "isExam": True,
        "egzaminMaxPkt": 20,
        "egzaminMinPkt": 10,
        "isProject": False,
        "projektMaxPkt": 0,
        "projektMinPkt": 0,
    }
    return body


@pytest.fixture
def incorrect_class_request_body(scope="function"):
    body = {
        "class_name": "Valid",
        "class_semester": "Letni",
        "class_year": 1997,
        "isExam": "True",
        "egzaminMaxPkt": 20,
        "egzaminMinPkt": 10,
        "isProject": False,
        "projektMaxPkt": 0,
        "projektMinPkt": 0,
    }
    return body


# Need to change this id after runing test on delete from test_class_repo.py
@pytest.fixture
def correct_class_id(scope="function"):
    valid_id = "RzgUO0GD3PjFcEWlVodE"
    return valid_id


@pytest.fixture
def incorrect_class_id(scope="function"):
    valid_id = "xxxxxxxxxxxxxxxxxxxxxx"
    return valid_id


# nalezy zmieniac po kazdym tescie na dodanie studenta
@pytest.fixture
def correct_student_request_body(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "email": "poprawny11s99@email.pl",
        "imie": "PoprawneImie",
        "nazwisko": "PoprwaneNazwisko",
    }
    return body


@pytest.fixture
def incorrect_student_request_body(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "email": "poprawny@emil.pl",
        "imie": 123,
        "nazwisko": "PoprwaneNazwisko",
    }
    return body


@pytest.fixture
def correct_patch_student_request_body(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "email": "patch@emil.pl",
        "name": "PatchImie",
        "lastName": "PatchNazwisko",
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
    }
    return body


@pytest.fixture
def incorrect_patch_student_request_body(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "email": "patch@emil.pl",
        "name": 90,
        "lastName": "PatchNazwisko",
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
    }
    return body


@pytest.fixture
def correct_exercise_request(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
        "exercise_desc": "Zadanie 1",
        "student_points": 34,
    }
    return body


@pytest.fixture
def incorrect_exercise_request(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
        "exercise_desc": 900,
        "student_points": 34,
    }
    return body


@pytest.fixture
def correct_exercise_for_class_request(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "exercise_desc": "Zadanie z testów00",
        "maxPkt": 999,
    }
    return body


@pytest.fixture
def incorrect_exercise_for_class_request(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "exercise_desc": "Nieistniejace",
        "maxPkt": "900",
    }
    return body


@pytest.fixture
def correct_pass_criteria_request(scope="function"):
    body = {
        "class_id": "05lKi6RjKhO6b0XjKMyD",
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
        "student_points": 40,
    }
    return body


@pytest.fixture
def incorrect_pass_criteria_request(scope="function"):
    body = {
        "class_id": 90,
        "student_id": "wRdGEWdLK0Y5Guxs9k9N",
        "student_points": 40,
    }
    return body
