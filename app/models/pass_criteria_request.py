from service.request_validation import RequestValidation


class ValidatedPassCriteriaRequest:
    def __init__(self, body):
        self.body = body

    def is_valid(self):
        if isinstance(self.body, dict) != True:
            return False
        rv = RequestValidation()
        if "class_id" not in self.body:
            return "No class_id was provided", 422
        if "student_id" not in self.body:
            return "No student_id was provided", 422
        if "student_points" not in self.body:
            return "No student_points was provided", 422

        class_id = self.body.get("class_id")
        student_id = self.body.get("student_id")
        isValid_class_id, communicat1 = rv.check_id_parameter_for_request(class_id)
        isValid_student_id, communicat2 = rv.check_id_parameter_for_request(student_id)

        if (
            isValid_class_id == True
            and isValid_student_id == True
            and type(self.body.get("student_points")) == int
        ):
            return True
        else:
            return False
