SEMESTER_TYPES = ["Letni", "Zimowy"]


class ValidatedClassRequest:
    def __init__(self, body):
        self.body = body

    def is_valid(self):
        if isinstance(self.body, dict) != True:
            return False
        if "class_name" not in self.body:
            return "No class_name was provided", 422
        if "class_semester" not in self.body:
            return "No class_semester was provided", 422
        if "class_year" not in self.body:
            return "No class_year was provided", 422
        if "isExam" not in self.body:
            return "No isExam was provided", 422
        if "exam_max_pts" not in self.body:
            return "No exam_max_pts was provided", 422
        if "exam_min_pts" not in self.body:
            return "No exam_min_pts was provided", 422
        if "isProject" not in self.body:
            return "No isProject was provided", 422
        if "project_max_pts" not in self.body:
            return "No project_max_pts was provided", 422
        if "project_min_pts" not in self.body:
            return "No project_min_pts was provided", 422

        if (
            type(self.body.get("class_name")) == str
            and type(self.body.get("class_year")) == int
            and self.check_semestr_value(self.body.get("class_semester"))
            and type(self.body.get("class_year")) == int
            and type(self.body.get("isExam")) == bool
            and type(self.body.get("exam_max_pts")) == int
            and type(self.body.get("exam_min_pts")) == int
            and type(self.body.get("isProject")) == bool
            and type(self.body.get("project_max_pts")) == int
            and type(self.body.get("project_min_pts")) == int
        ):
            return True
        else:
            return False

    def check_semestr_value(self, word):
        """
        :param word: string
        :return: bool
        """
        if word in SEMESTER_TYPES:
            return True
        else:
            return False
