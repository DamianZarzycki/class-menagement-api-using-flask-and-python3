from service.request_validation import RequestValidation


class ValidatedStudentRequest:
    def __init__(self, body):
        self.body = body

    def is_valid(self):
        rv = RequestValidation()
        if isinstance(self.body, dict) != True:
            return False
        if "class_id" not in self.body:
            return "No class_id was provided", 422
        if "email" not in self.body:
            return "No email was provided", 422
        if "name" not in self.body:
            return "No name was provided", 422
        if "last_name" not in self.body:
            return "No last_name was provided", 422

        isValid = rv.check_id_parameter_for_request(self.body.get("class_id"))

        if (
            isValid[0]
            and type(self.body.get("email")) == str
            and type(self.body.get("name")) == str
            and type(self.body.get("last_name")) == str
        ):
            return True
        else:
            return False

    def is_valid_patch_request(self):
        rv = RequestValidation()

        if not isinstance(self.body, dict):
            return False
        if "class_id" not in self.body:
            return "No class_id was provided", 422
        if "email" not in self.body:
            return "No email was provided", 422
        if "name" not in self.body:
            return "No name was provided", 422
        if "last_name" not in self.body:
            return "No last_name was provided", 422
        if "student_id" not in self.body:
            return "No student_id was provided", 422

        # isValid_class_id = rv.check_id_parameter_for_request(self.body.get("class_id"))
        # isValid_student_id = rv.check_id_parameter_for_request(
        #     self.body.get("student_id")
        # )

        if (
            # isValid_class_id[0]
            # and isValid_student_id[0]
            type(self.body.get("email")) == str
            and type(self.body.get("name")) == str
            and type(self.body.get("last_name")) == str
        ):
            return True
        else:
            return False
