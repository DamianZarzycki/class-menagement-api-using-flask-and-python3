from service.request_validation import RequestValidation


class ValidatedExerciseRequest:
    def __init__(self, body):
        self.body = body

    def is_valid(self):
        rv = RequestValidation()
        if isinstance(self.body, dict) != True:
            return False
        if "class_id" not in self.body:
            return "No class_id was provided", 422
        if "student_id" not in self.body:
            return "No student_id was provided", 422
        if "exercise_desc" not in self.body:
            return "No exercise_desc was provided", 422
        if "student_points" not in self.body:
            return "No student_points was provided", 422

        if (
            rv.check_id_parameter_for_request(self.body.get("class_id"))
            and rv.check_id_parameter_for_request(self.body.get("student_id"))
            and type(self.body.get("exercise_desc")) == str
            and type(self.body.get("student_points")) == int
        ):
            return True
        else:
            return False


class ValidatedExerciseForClassRequest:
    def __init__(self, body):
        self.body = body

    def is_valid(self):
        rv = RequestValidation()
        if not isinstance(self.body, dict):
            return False
        if "class_id" not in self.body:
            return "No class_id was provided", 422
        if "exercise_desc" not in self.body:
            return "No exercise_desc was provided", 422
        if "maxPkt" not in self.body:
            return "No maxPkt was provided", 422

        if (
            rv.check_id_parameter_for_request(self.body.get("class_id"))
            and type(self.body.get("exercise_desc")) == str
            and type(self.body.get("maxPkt")) == int
        ):
            return True
        else:
            return False
