from flask import jsonify
from database import collection


class StudentRepository:
    classRoom = collection("students_class")

    def delete_student_from_class(self, student_id, class_id):
        """
        :param student_id: string
        :param class_id: string
        :return: tuple(string, number)
        """
        return self._delete_student_from_class_from_firestore(student_id, class_id)

    def add_class(self, student_to_add):
        """
        :param student_to_add: dict
        :return: tuple(string, number)
        """
        return self._add_class_from_firestore(student_to_add)

    def get_specyfic_student_from_class(self, student_id, class_id):
        """
        :param student_id: string
        :param class_id: string
        :return: tuple(dict, number)
        """
        return self._get_specyfic_student_from_class_from_firestore(
            student_id, class_id
        )

    def get_students_from_class(self, class_id):
        """
        :param class_id: string
        :return: tuple(list, number)
        """
        return self._get_students_from_class_from_firestore(class_id)

    def update_student(self, data_to_update):
        """
        :param data_to_update: dict
        :return: tuple(list, number)
        """
        return self._update_student_from_firestore(data_to_update)

    def _get_specyfic_student_from_class_from_firestore(self, student_id, class_id):
        """
        :param student_id: string
        :param class_id: string
        :return: tuple(dict, number)
        """
        student = (
            self.classRoom.document(class_id)
                .collection("students")
                .document(student_id)
                .get()
                .exists
        )

        if student:
            student = (
                self.classRoom.document(class_id)
                    .collection("students")
                    .document(student_id)
                    .get()
                    .to_dict()
            )
            return jsonify(student), 200
        else:
            return "There is no student in this class with this id", 422

    def _delete_student_from_class_from_firestore(self, student_id, class_id):
        """
        :param student_id: string
        :param class_id: string
        :return: tuple(string, number)
        """
        student = (
            self.classRoom.document(class_id)
                .collection("students")
                .document(student_id)
                .get()
                .exists
        )
        if student:
            self.classRoom.document(class_id).collection("students").document(
                student_id
            ).delete()
            return {'message': 'Resource deleted successfully'}, 200
        else:
            return {'message': 'There is no such student in this class'}, 422

    def _add_class_from_firestore(self, student_to_add):
        """
        :param student_to_add: dict
        :return: tuple(string, number)
        """
        class_id = student_to_add.get("class_id")
        student_email = student_to_add.get("email")
        query_ref = (
            self.classRoom.document(class_id)
                .collection(u"students")
                .where("email", "==", student_email)
        )
        doc = query_ref.stream()

        for d in doc:
            if student_email == d.to_dict()["email"]:
                return {'message': 'There is already student with this email'}, 409
        pass_criteria = (
            self.classRoom.document(class_id).get().to_dict()["pass_criteria"]
        )

        exam = pass_criteria.get("exam")
        project = pass_criteria.get("project")

        # Upewnienie się, że typ danych przesłany do DB będzie ten odpowieni dla konkretnych kryteriów
        zaliczenia = dict((el, {}) for el in pass_criteria)
        if zaliczenia["exercise"] == {}:
            zaliczenia["exercise"] = []

        if exam:
            if zaliczenia["exam"] == {}:
                zaliczenia["exam"] = pass_criteria["exam"]
                zaliczenia["exam"].update({"student_points": 0})
        else:
            pass

        if project:
            if zaliczenia["project"] == {}:
                zaliczenia["project"] = pass_criteria["project"]
                zaliczenia["project"].update({"student_points": 0})
        else:
            pass

        data = {
            "name": student_to_add.get("name"),
            "last_name": student_to_add.get("last_name"),
            "email": student_to_add.get("email"),
        }

        zaliczenia["exercise"] = pass_criteria["exercise"]
        for zc in zaliczenia["exercise"]:
            zc.update({"student_points": 0})
        data.update(zaliczenia)

        self.classRoom.document(class_id).collection("students").add(data)

        return {'message': 'Success'}, 201

    def _get_students_from_class_from_firestore(self, class_id):
        """
        :param class_id: string
        :return: tuple(list, number)
        """
        class_exist = self.classRoom.document(class_id).get().exists
        if class_exist:
            students = (
                self.classRoom.document(class_id).collection("students").stream()
            )
            studentsArr = []
            for student in students:
                data = {
                    "student_id": student.id,
                }
                data.update(student.to_dict())
                studentsArr.append(data)
            return jsonify(studentsArr), 200
        else:
            return "Class with this id does not exist", 422

    def _update_student_from_firestore(self, data_to_update):
        """
        :param data_to_update: dict
        :return: tuple(string, number)
        """
        student = (
            self.classRoom.document(data_to_update.get("class_id"))
                .collection("students")
                .document(data_to_update.get("student_id"))
                .get()
                .exists
        )
        if student:
            body = data_to_update
            name_to_update = body.get("name")
            lastName_to_update = body.get("last_name")
            email_to_update = body.get("email")
            class_id = body.get("class_id")
            student_id = body.get("student_id")

            student = (
                self.classRoom.document(class_id)
                    .collection("students")
                    .document(student_id)
                    .get()
                    .to_dict()
            )

            email = student.get("email")
            name = student.get("name")
            lastName = student.get("last_name")

            if name_to_update:
                name = name_to_update

            if lastName_to_update:
                lastName = lastName_to_update

            if email_to_update:
                email = email_to_update

            data = {"email": email, "name": name, "last_name": lastName}

            self.classRoom.document(class_id).collection("students").document(
                student_id
            ).update(data)

            return {'message': 'Success'}, 201
        else:
            return {'message': 'There is no such student in this class'}, 422
