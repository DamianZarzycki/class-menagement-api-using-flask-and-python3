import json
from google.cloud import firestore
from flask import Flask, request, jsonify, make_response
from database import collection


class ClassRepository:
    classRoom = collection("students_class")

    def get_class(self, ids):
        """
        :param ids: string
        :return: tuple(dict, number)
        """
        return self._get_from_firestore(ids)

    def get_classes(self):
        """
        :return: tuple(dict, number)
        """
        return self._get_classes_from_firestore()

    def add_class(self, class_body):
        """
        :param class_body: dict
        :return: tuple(string, number)
        """
        return self._add_to_firestore(class_body)

    def delete_class(self, ids):
        """
        :param ids: string
        :return: tuple(string, number)
        """

        return self._delete_from_firestore(ids)

    def _delete_from_firestore(self, ids):
        """
        :param ids: string
        :return: tuple(string,number)
        """
        is_class_exist = self.classRoom.document(ids).get().exists
        if is_class_exist:
            self.classRoom.document(ids).delete()
            return {'message':'Resource deleted successfully'}, 200
        else:
            return {'message':'Class with that id does not exist'}, 404

    def _get_classes_from_firestore(self):
        """
        :return: tuple(dict, number)
        """
        classArr = []
        for c in self.classRoom.stream():
            classArr.append(
                {"class_id": c.id, "data": c.to_dict()}
            )
        return jsonify(classArr), 200

    def _get_from_firestore(self, ids):
        """
        :param ids: string
        :return: tuple(string,number)
        """
        is_class_details_exist = self.classRoom.document(ids).get().exists
        if is_class_details_exist:
            class_details = self.classRoom.document(ids).get().to_dict()
            students = self.classRoom.document(ids).collection("students").stream()
            studentsArr = []
            for student in students:
                data = {
                    "student_id": student.id,
                }
                data.update(student.to_dict())
                studentsArr.append(data)
            response = {
                "class_id": ids,
                "class_details": class_details,
                "students": studentsArr,
            }
            return jsonify(response), 200
        else:
            communicat = "Class of provided id does not exist"
            return communicat, 404

    def _add_to_firestore(self, class_body):
        """
        :param class_body: dict
        :return: tuple(string,number)
        """
        data = class_body
        class_name = data.get("class_name")
        class_year = data.get("class_year")
        class_semester = data.get("class_semester")
        exercise = {"exercise": []}
        if data.get("isExam") == True:
            exercise.update(
                {
                    "exam": {
                        "max_pts": data.get("exam_max_pts"),
                        "min_pts": data.get("exam_min_pts"),
                    }
                }
            )
        if data.get("isProject") == True:
            exercise.update(
                {
                    "project": {
                        "max_pts": data.get("project_max_pts"),
                        "min_pts": data.get("project_max_pts"),
                    }
                }
            )
        data_to_add = {
            "name": class_name,
            "year": class_year,
            "semester": class_semester,
            "pass_criteria": exercise,
        }
        self.classRoom.add(data_to_add)

        return {'message': 'Success'}, 201
