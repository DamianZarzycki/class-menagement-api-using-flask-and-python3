from database import collection
from flask import Flask, request, jsonify, make_response


class ExerciseRepository:
    classRoom = collection("students_class")

    def add_exercise_for_student(self, body):
        """
        :param body: dict
        :return: tuple(string,number)
        """
        return self._add_exercise_for_student_from_firestore(body)

    def add_exercise_to_class(self, body):
        """
        :param body: dict
        :return: tuple(string,number)
        """
        return self._add_exercise_to_class_from_firestore(body)

    def get_exercise_from_class(self, class_id):
        """
        :param class_id: string
        :return: tuple(string,number)
        """
        return self._get_exercise_from_class_from_firebase(class_id)

    def _add_exercise_for_student_from_firestore(self, body):
        """
        :param body: dict
        :return: tuple(string,number)
        """
        data = body
        class_id = data.get("class_id")
        student_id = data.get("student_id")
        exercise_description = data.get("exercise_desc")
        student_points = data.get("student_points")

        cwiczenia = (
            # self.classRoom.document.collection("uczniowie").document(student_id).get().to_dict()["cwiczenia"]
            self.classRoom.document(class_id)
            .collection("students")
            .document(student_id)
            .get()
            .to_dict()["exercise"]
        )
        cwiczeniaClass = (
            self.classRoom.document(class_id).get().to_dict()["pass_criteria"]
        )

        cwiczeniaClass["exercise"]
        exercise = []
        for cw in cwiczeniaClass["exercise"]:
            exercise.append(cw["description"])

        if exercise_description not in exercise:
            return "Takie cwiczenie nie zostało dodane do klasy", 409

        for cw in cwiczenia:
            if exercise_description == cw["description"]:
                cw["student_points"] = student_points

        data_to_update = {
            "exercise": cwiczenia,
        }
        self.classRoom.document(class_id).collection("students").document(
            student_id
        ).update(data_to_update)
        return {'message': 'Success'}, 201

    def _add_exercise_to_class_from_firestore(self, body):
        """
        :param body: dict
        :return: tuple(string,number)
        """
        data = body
        class_id = data.get("class_id")
        maxPkt = data.get("max_pts")
        exercise_description = data.get("exercise_desc")
        cwiczeniaClass = (
            self.classRoom.document(class_id).get().to_dict()["pass_criteria"]
        )

        cwiczeniaClass["exercise"]
        uczniowie = self.classRoom.document(class_id).collection("students")

        exercise = []
        for cw in cwiczeniaClass["exercise"]:
            if cw["description"] == exercise_description:
                maxPkt = cw["max_pts"]
            exercise.append(cw["description"])

        if exercise_description in exercise:
            return "Exercise like that was already added", 409

        for u in uczniowie.stream():
            cwiczenia = (
                self.classRoom.document(class_id)
                .collection("students")
                .document(u.id)
                .get()
                .to_dict()["exercise"]
            )
            cwiczenia.append(
                {"description": exercise_description, "student_points": 0, "max_pts": maxPkt}
            )
            data_to_update = {
                "exercise": cwiczenia,
            }

            self.classRoom.document(class_id).collection("students").document(
                u.id
            ).update(data_to_update)

        cwiczenia = (
            self.classRoom.document(class_id).get().to_dict()[u"pass_criteria"]
        )
        project = cwiczenia.get("project")
        exam = cwiczenia.get("exam")

        pass_criteria = {
            "exercise": cwiczenia["exercise"],
        }

        if project:
            pass_criteria.update({"project": project})

        if exam:
            pass_criteria.update({"exam": exam})

        cwiczenia["exercise"].append({"max_pts": maxPkt, "description": exercise_description})
        dataToUpdate = {
            "pass_criteria": pass_criteria,
        }
        self.classRoom.document(class_id).update(dataToUpdate)
        return {'message': 'Success'}, 201

    def _get_exercise_from_class_from_firebase(self, class_id):
        """
        :param class_id: string
        :return: tuple(string,number)
        """
        exercise = self.classRoom.document(class_id).get().exists
        if exercise:
            pass_criteria = (
                self.classRoom.document(class_id).get().to_dict()[u"pass_criteria"]
            )
            exercise = pass_criteria.get("exercise")

            if exercise:
                return jsonify(exercise), 200
            else:
                return "There is no exercise for this class"
        else:
            return "Class with that id does not exist", 422
