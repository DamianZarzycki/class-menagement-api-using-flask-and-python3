class RequestValidation:
    def check_id_parameter_for_request(self, provided_id):
        """
        :param provided_id: string
        :return: tuple(bool, string)
        """
        if type(provided_id) == int:
            return False, "Provided id must be a string"
        elif provided_id != "false" and provided_id != "true" and type(provided_id) == str:
            return True, "Everything good"
        else:
            return False, "Provided id must be a string"

    def check_existance_of_student_and_class_id_url_parameters(self, request):
        """
        :param request: dict
        :return: tuple(string, number)
        """
        class_id = request.get("class_id")
        student_id = request.get("student_id")

        if class_id is None and student_id is None:
            return "Please provide class_id and student_id parameters", 400
        elif class_id is None:
            return "Please provide class_id parameter", 400
        elif student_id is None:
            return "Please provide student_id parameter", 400
        else:
            pass
