import json
from google.cloud import firestore
from flask import Flask, request, jsonify, make_response


class PassCriteriaRepository:
    db = firestore.Client()
    classRoom = db.collection("students_class")

    def mark_studetn_project(self, body):
        """
        :param body: dict
        :return: tuple(string, number)
        """
        return self._mark_student_project_from_firestore(body)

    def mark_student_exam(self, body):
        """
        :param body: dict
        :return: tuple(string, number)
        """
        return self._mark_student_exam_from_firestore(body)

    def _mark_student_project_from_firestore(self, body):
        """
        :param body: dict
        :return: tuple(string, number)
        """
        data = body
        class_id = data.get("class_id")
        student_id = data.get("student_id")
        student_points = data.get("student_points")
        classPassCriteria = (
            self.classRoom.document(class_id).get().to_dict()["pass_criteria"]
        )

        classProjectDetails = classPassCriteria.get("project")

        if classProjectDetails:
            project = (
                self.classRoom.document(class_id)
                .collection("students")
                .document(student_id)
                .get()
                .to_dict()["project"]
            )

            maxPkt = classProjectDetails.get("max_pts")
            if int(maxPkt) < int(student_points):
                return (
                    "Can not add more points than maximum",
                    400,
                )

            project.update({"student_points": student_points})
            data_to_update = {"project": project}
            self.classRoom.document(class_id).collection("students").document(
                student_id
            ).update(data_to_update)

            return {'message': 'Success'}, 201
        else:
            return "This class does not have project as pass criteria", 400

    def _mark_student_exam_from_firestore(self, body):
        """
        :param body: dict
        :return: tuple(string, number)
        """
        data = body
        class_id = data.get("class_id")
        student_id = data.get("student_id")
        student_points = data.get("student_points")
        classPassCriteria = (
            self.classRoom.document(class_id).get().to_dict()["pass_criteria"]
        )

        classProjectDetails = classPassCriteria.get("exam")

        if classProjectDetails:
            exam = (
                self.classRoom.document(class_id)
                .collection("students")
                .document(student_id)
                .get()
                .to_dict()["exam"]
            )

            maxPkt = classProjectDetails.get("max_pts")
            if int(maxPkt) < int(student_points):
                return (
                    "Can not add more points than maximum",
                    400,
                )

            exam.update({"student_points": student_points})
            data_to_update = {"exam": exam}
            self.classRoom.document(class_id).collection("students").document(
                student_id
            ).update(data_to_update)

            return {'message': 'Success'}, 201
        else:
            return "This class does not have exam as pass criteria", 400
