from main import app
from utils import common
import json


class TestStudentRepository:
    def test_given_correct_class_id_and_student_id_when_requesting_specyfic_student_details_then_checking_its_details(
        self, correct_class_id_and_student_id_combination
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/student",
            query_string={
                "class_id": correct_class_id_and_student_id_combination.get("class_id"),
                "student_id": correct_class_id_and_student_id_combination.get(
                    "student_id"
                ),
            },
        )
        response_data = common.decode_and_load_to_json(resp.data)
        status_code = resp.status_code

        assert "cwiczenia" in response_data
        assert "imie" in response_data
        assert "nazwisko" in response_data
        assert "email" in response_data
        assert status_code == 200

    def test_given_correct_class_id_when_requesting_students_from_that_class_then_expect_200_status_code_and_a_list(
        self, correct_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/students", query_string={"class_id": correct_class_id},
        )
        response_data = common.decode_and_load_to_json(resp.data)
        status_code = resp.status_code
        assert type(response_data) == list
        assert status_code == 200

    def test_given_class_id_of_not_existing_class_and_not_providing_student_id_when_requesting_specyfic_student_details_then_expect_to_get_400_and_communicat_about_missing_parameter(
        self, incorrect_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/student", query_string={"class_id": incorrect_class_id}
        )
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Please provide student_id parameter"
        assert response_status_code == 400

    def test_given_not_existing_student_id_and_not_providing_class_id_when_requesting_specyfic_student_details_then_expect_to_get_400_and_communicat_about_missing_parameter(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get("api/class/student", query_string={"student_id": "90"})
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Please provide class_id parameter"
        assert response_status_code == 400

    def test_given_no_class_id_and_student_id_url_parameters_when_requesting_specyfic_class_details_then_expect_to_get_400_and_communicat_about_providing_both_parameters(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get("api/class/student")
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert (
            response_data_decoded == "Please provide class_id and student_id parameters"
        )
        assert response_status_code == 400

    def test_given_correct_class_id_and_incorrect_student_id_url_parameters_when_requesting_specyfic_class_details_then_expect_to_get_400(
        self, correct_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/student",
            query_string={"class_id": correct_class_id, "student_id": 90},
        )
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "There is no student in this class with this id"
        assert response_status_code == 422

    def test_given_not_existing_class_id_when_trying_to_get_students_from_that_class_then_expect_422_status_code_and_communicat_about_not_existing_class(
        self, incorrect_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/students", query_string={"class_id": incorrect_class_id}
        )
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Class with this id does not exist"
        assert response_status_code == 422

    def test_given_correct_data_when_updating_student_in_class_then_expect_200_status_code_and_updated_data_to_be_the_same_as_provided_one(
        self, correct_patch_student_request_body
    ):
        tester = app.test_client(self)
        body = correct_patch_student_request_body
        resp = tester.patch("api/class/student", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        resp = tester.get(
            "api/class/student",
            query_string={
                "class_id": correct_patch_student_request_body.get("class_id"),
                "student_id": correct_patch_student_request_body.get("student_id"),
            },
        )

        get_response_data_decoded = common.decode_and_load_to_json(resp.data)
        updated_name = get_response_data_decoded.get("imie")
        updated_lastName = get_response_data_decoded.get("nazwisko")
        updated_email = get_response_data_decoded.get("email")

        assert updated_email == correct_patch_student_request_body.get("email")
        assert updated_name == correct_patch_student_request_body.get("name")
        assert updated_lastName == correct_patch_student_request_body.get("lastName")
        assert response_data_decoded == "Success"
        assert response_status_code == 200

    def test_given_incorrect_data_when_updating_student_in_class_then_expect_422_status_code(
        self, incorrect_patch_student_request_body
    ):
        tester = app.test_client(self)
        body = incorrect_patch_student_request_body
        resp = tester.patch("api/class/student", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422

    def test_given_no_data_when_updating_student_in_class_then_expect_422_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.patch("api/class/student", json=None)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422

    def test_given_valid_data_when_posting_student_to_new_class_then_expect_200_status_code(
        self, correct_student_request_body
    ):
        tester = app.test_client(self)
        body = correct_student_request_body
        resp = tester.post("api/class/students", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Success"
        assert response_status_code == 200

    def test_given_valid_data_when_posting_second_time_the_same_student_to_new_class_then_expect_403_status_code(
        self, correct_student_request_body
    ):
        tester = app.test_client(self)
        body = correct_student_request_body
        resp = tester.post("api/class/students", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert (
            response_data_decoded == "Student o tym mailu już istnieje w obecnej klasie"
        )
        assert response_status_code == 403

    def test_given_invalid_data_when_posting_student_to_new_class_then_expect_422_status_code(
        self, incorrect_student_request_body
    ):
        tester = app.test_client(self)
        body = incorrect_student_request_body
        resp = tester.post("api/class/students", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422

    def test_given_no_data_when_posting_student_to_new_class_then_expect_422_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.post("api/class/students", json=None)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422

    def test_given_existing_class_id_and_student_id_when_deleting_student_from_class_then_expect_200_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/students", query_string={"class_id": "05lKi6RjKhO6b0XjKMyD"}
        )
        response_data_len_before = len(common.decode_and_load_to_json(resp.data))
        resp = tester.delete(
            "api/class/student",
            query_string={
                "class_id": "05lKi6RjKhO6b0XjKMyD",
                "student_id": "UR47WVfteQHC838PNtd4",
            },
        )
        deletion_status_code = resp.status_code
        resp = tester.get(
            "api/class/students", query_string={"class_id": "05lKi6RjKhO6b0XjKMyD"}
        )
        response_data_len_after = len(common.decode_and_load_to_json(resp.data))

        assert deletion_status_code == 200
        assert response_data_len_after == response_data_len_before - 1

    def test_given_existing_class_id_and_student_id_of_student_that_does_not_exist_when_deleting_student_from_class_then_expect_422_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/students", query_string={"class_id": "05lKi6RjKhO6b0XjKMyD"}
        )
        response_data_len_before = len(common.decode_and_load_to_json(resp.data))
        resp = tester.delete(
            "api/class/student",
            query_string={
                "class_id": "05lKi6RjKhO6b0XjKMyD",
                "student_id": "UR47WVfteQHC838PNtd4",
            },
        )
        deletion_status_code = resp.status_code
        resp = tester.get(
            "api/class/students", query_string={"class_id": "05lKi6RjKhO6b0XjKMyD"}
        )
        response_data_len_after = len(common.decode_and_load_to_json(resp.data))

        assert deletion_status_code == 422
        assert response_data_len_after == response_data_len_before
