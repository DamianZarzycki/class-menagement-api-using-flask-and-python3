from main import app
from utils import common
import json


class TestClassRepository:
    def test_given_correct_class_id_when_requesting_specyfic_class_details_then_checking_its_details(
        self, correct_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get("api/class", query_string={"class_id": correct_class_id})
        response_data = common.decode_and_load_to_json(resp.data)

        assert "students" in response_data
        assert "class_details" in response_data
        assert "name" in response_data.get("class_details")
        assert "rok" in response_data.get("class_details")
        assert "semestr" in response_data.get("class_details")
        assert "class_id" in response_data
        assert response_data.get("class_id") == correct_class_id

    def test_given_class_id_of_not_existing_class_when_requesting_specyfic_class_details_then_expect_to_get_404(
        self, incorrect_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get("api/class", query_string={"class_id": incorrect_class_id})
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Class of provided id does not exist"
        assert response_status_code == 404

    def test_given_enough_data_when_making_request_for_list_of_classes_then_expect_to_get_a_list_and_200_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get("api/classes")
        status_code = resp.status_code
        response_data = common.decode_and_load_to_json(resp.data)

        assert status_code == 200
        assert type(response_data) == list

    def test_given_valid_data_when_posting_new_class_then_expect_200_status_code_and_one_more_class_in_db(
        self, correct_class_request_body
    ):
        tester = app.test_client(self)
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        resp = tester.get("api/classes")
        response_data_len_before = len(common.decode_and_load_to_json(resp.data))
        body = correct_class_request_body
        response = tester.post("api/class", data=json.dumps(body), headers=headers)
        resp = tester.get("api/classes")
        response_data_len_after = len(common.decode_and_load_to_json(resp.data))
        status_code = response.status_code

        assert status_code == 200
        assert response_data_len_after == response_data_len_before + 1

    def test_given_invalid_data_when_posting_new_class_then_expect_422_status_code_and_the_same_amount_of_classes_before_request(
        self, incorrect_class_request_body
    ):
        tester = app.test_client(self)
        resp = tester.get("api/classes")
        response_data_len_before = len(common.decode_and_load_to_json(resp.data))
        response = tester.post(
            "api/class",
            data=json.dumps(incorrect_class_request_body),
            content_type="application/json",
        )

        resp = tester.get("api/classes")
        response_data_len_after = len(common.decode_and_load_to_json(resp.data))
        status_code = response.status_code

        assert status_code == 422
        assert response_data_len_after == response_data_len_before

    # def test_given_correct_class_id_when_deleting_class_then_expect_200_status_code_and_one_less_class_in_db(
    #     self, correct_class_id
    # ):
    #     tester = app.test_client(self)
    #     resp = tester.get("api/classes")
    #     response_data_len_before = len(common.decode_and_load_to_json(resp.data))
    #     resp = tester.delete("api/class", query_string={"class_id": correct_class_id})
    #     deletion_status_code = resp.status_code
    #     resp = tester.get("api/classes")
    #     response_data_len_after = len(common.decode_and_load_to_json(resp.data))

    #     assert deletion_status_code == 200
    #     assert response_data_len_after == response_data_len_before - 1

    def test_given_incorrect_class_id_when_deleting_class_then_expect_404_status_code_and_the_same_amount_of_classes_in_db_and_response_that_this_class_does_not_exist(
        self, incorrect_class_id
    ):
        tester = app.test_client(self)
        resp = tester.get("api/classes")
        response_data_len_before = len(common.decode_and_load_to_json(resp.data))
        resp = tester.delete("api/class", query_string={"class_id": incorrect_class_id})
        deletion_status_code = resp.status_code
        response_data = resp.data.decode("utf-8")
        resp = tester.get("api/classes")
        response_data_len_after = len(common.decode_and_load_to_json(resp.data))

        assert response_data == "Class with that id does not exist"
        assert deletion_status_code == 404
        assert response_data_len_after == response_data_len_before
