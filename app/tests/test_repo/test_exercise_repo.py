from main import app
from utils import common
import json


class TestExerciseRepository:
    def test_given_valid_class_id_when_reqesting_class_exercise_then_expect_list_of_exercises_and_200_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/exercise", query_string={"class_id": "05lKi6RjKhO6b0XjKMyD"},
        )
        response_data = common.decode_and_load_to_json(resp.data)
        status_code = resp.status_code

        assert type(response_data) == list
        assert status_code == 200

    def test_given_invalid_class_id_when_reqesting_class_exercise_then_expect_422_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get(
            "api/class/exercise", query_string={"class_id": "05lKi6RjKhO6b0XjKMyDs"},
        )
        response_data = resp.data.decode()
        status_code = resp.status_code

        assert response_data == "Class with that id does not exist"
        assert status_code == 422

    def test_given_no_parameters_when_reqesting_class_exercise_then_422_status_code_and_communicat_about_missing_argument(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.get("api/class/exercise")
        response_data = resp.data.decode()
        status_code = resp.status_code
        assert response_data == "Please provide class_id parameter"
        assert status_code == 400

    def test_given_valid_exercise_reqest_body_when_adding_exercise_to_class_then_expect_200_status_code(
        self, correct_exercise_for_class_request
    ):
        tester = app.test_client(self)
        body = correct_exercise_for_class_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/exercise", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Success"
        assert status_code == 200

    def test_given_valid_exercise_reqest_body_when_adding_already_added_exercise_to_class_then_expect_200_status_code(
        self, correct_exercise_for_class_request
    ):
        tester = app.test_client(self)
        body = correct_exercise_for_class_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/exercise", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Takie cwiczenie zostało juz dodane do klasy"
        assert status_code == 409

    def test_given_invalid_body_format_when_trying_to_add_the_exercise_to_the_class_then_expect_422_status_code(
        self, correct_exercise_for_class_request
    ):
        tester = app.test_client(self)
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post("api/class/exercise", json="f", headers=headers)
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Check types of parameters"
        assert status_code == 422

    def test_given_valid_exercise_request_body_when_grading_student_exercise_then_expect_200_status_code(
        self, correct_exercise_request
    ):
        tester = app.test_client(self)
        body = correct_exercise_request
        resp = tester.patch("api/class/students/exercise", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Success"
        assert response_status_code == 200

    def test_given_invalid_exercise_request_body_when_grading_student_exercise_then_expect_422_status_code(
        self, incorrect_exercise_request
    ):
        tester = app.test_client(self)
        body = incorrect_exercise_request
        resp = tester.patch("api/class/students/exercise", json=body)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422

    def test_given_no_exercise_request_body_when_grading_student_exercise_then_expect_422_status_code(
        self,
    ):
        tester = app.test_client(self)
        resp = tester.patch("api/class/students/exercise", json=None)
        response_data_decoded = resp.data.decode("utf-8")
        response_status_code = resp.status_code

        assert response_data_decoded == "Check types of parameters"
        assert response_status_code == 422
