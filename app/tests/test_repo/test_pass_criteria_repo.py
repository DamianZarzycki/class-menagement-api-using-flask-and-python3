from main import app
from utils import common
import json


class TestPassCriteriaRepository:
    def test_given_valid_body_when_grading_student_project_then_expect_200_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/project", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Success"
        assert status_code == 200

    def test_given_valid_body_when_grading_student_exam_then_expect_200_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/exam", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Success"
        assert status_code == 200

    def test_given_to_high_amount_of_points_when_grading_student_project_then_expect_400_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        correct_pass_criteria_request["student_points"] = 99999
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/project", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert (
            response_data
            == "Nie można wpisać więcej punktów niż maksimum punktów do zdobycia"
        )
        assert status_code == 400

    def test_given_to_high_amount_of_points_when_grading_student_exam_then_expect_400_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        correct_pass_criteria_request["student_points"] = 99999
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/exam", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert (
            response_data
            == "Nie można wpisać więcej punktów niż maksimum punktów do zdobycia"
        )
        assert status_code == 400

    def test_given_no_amount_of_students_points_when_grading_student_project_then_expect_422_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        correct_pass_criteria_request["student_points"] = None
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/project", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Check types of parameters"
        assert status_code == 422

    def test_given_no_amount_of_students_points_when_grading_student_exam_then_expect_422_status_code(
        self, correct_pass_criteria_request
    ):
        tester = app.test_client(self)
        correct_pass_criteria_request["student_points"] = None
        body = correct_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/exam", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Check types of parameters"
        assert status_code == 422

    def test_given_invalid_body_when_grading_student_project_then_expect_200_status_code(
        self, incorrect_pass_criteria_request
    ):
        tester = app.test_client(self)
        body = incorrect_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/project", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Check types of parameters"
        assert status_code == 422

    def test_given_invalid_body_when_grading_student_exam_then_expect_200_status_code(
        self, incorrect_pass_criteria_request
    ):
        tester = app.test_client(self)
        body = incorrect_pass_criteria_request
        mimetype = "application/json"
        headers = {"Content-Type": mimetype, "Accept": mimetype}
        response = tester.post(
            "api/class/students/exam", data=json.dumps(body), headers=headers
        )
        status_code = response.status_code
        response_data = response.data.decode("UTF-8")

        assert response_data == "Check types of parameters"
        assert status_code == 422
