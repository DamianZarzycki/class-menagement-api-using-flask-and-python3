from models.exercise_request import (
    ValidatedExerciseForClassRequest,
    ValidatedExerciseRequest,
)


class TestExerciseRequestValidator:
    def test_given_exercise_request_validator_correct_body_then_expect_true(
        self, correct_exercise_request
    ):
        ver = ValidatedExerciseRequest(correct_exercise_request)
        result = ver.is_valid()
        assert result is True

    def test_given_exercise_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_exercise_request
    ):
        ver = ValidatedExerciseRequest(incorrect_exercise_request)
        result = ver.is_valid()
        assert result is False

    def test_given_exercise_request_validator_incorrect_argument_type_then_expect_false(
        self,
    ):
        ver = ValidatedExerciseRequest(True)
        result = ver.is_valid()
        assert result is False

    def test_given_exercise_request_validator_body_without_class_id_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_request
    ):
        correct_exercise_request.pop("class_id")
        vcr = ValidatedExerciseRequest(correct_exercise_request)
        communicat, status = vcr.is_valid()
        assert communicat == "No class_id was provided"
        assert status == 422

    def test_given_exercise_request_validator_body_without_student_id_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_request
    ):
        correct_exercise_request.pop("student_id")
        vcr = ValidatedExerciseRequest(correct_exercise_request)
        communicat, status = vcr.is_valid()
        assert communicat == "No student_id was provided"
        assert status == 422

    def test_given_exercise_request_validator_body_without_exercise_desc_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_request
    ):
        correct_exercise_request.pop("exercise_desc")
        vcr = ValidatedExerciseRequest(correct_exercise_request)
        communicat, status = vcr.is_valid()
        assert communicat == "No exercise_desc was provided"
        assert status == 422

    def test_given_exercise_request_validator_body_without_student_points_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_request
    ):
        correct_exercise_request.pop("student_points")
        vcr = ValidatedExerciseRequest(correct_exercise_request)
        communicat, status = vcr.is_valid()
        assert communicat == "No student_points was provided"
        assert status == 422


class TestExerciseForClassRequestValidator:
    def test_given_exercise_request_validator_correct_body_then_expect_true(
        self, correct_exercise_for_class_request
    ):
        vefcr = ValidatedExerciseForClassRequest(correct_exercise_for_class_request)
        result = vefcr.is_valid()
        assert result is True

    def test_given_exercise_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_exercise_for_class_request
    ):
        vefcr = ValidatedExerciseForClassRequest(incorrect_exercise_for_class_request)
        result = vefcr.is_valid()
        assert result is False

    def test_given_exercise_request_validator_body_without_class_id_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_for_class_request
    ):
        correct_exercise_for_class_request.pop("class_id")
        vefcr = ValidatedExerciseForClassRequest(correct_exercise_for_class_request)
        communicat, status = vefcr.is_valid()
        assert communicat == "No class_id was provided"
        assert status == 422

    def test_given_exercise_request_validator_body_without_exercise_desc_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_for_class_request
    ):
        correct_exercise_for_class_request.pop("exercise_desc")
        vefcr = ValidatedExerciseForClassRequest(correct_exercise_for_class_request)
        communicat, status = vefcr.is_valid()
        assert communicat == "No exercise_desc was provided"
        assert status == 422

    def test_given_exercise_request_validator_body_without_maxPkt_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_exercise_for_class_request
    ):
        correct_exercise_for_class_request.pop("maxPkt")
        vefcr = ValidatedExerciseForClassRequest(correct_exercise_for_class_request)
        communicat, status = vefcr.is_valid()
        assert communicat == "No maxPkt was provided"
        assert status == 422
