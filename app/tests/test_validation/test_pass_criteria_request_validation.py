from models.pass_criteria_request import ValidatedPassCriteriaRequest


class TestPassCriteriaRequestValidator:
    def test_given_pass_criteria_request_validator_correct_body_then_expect_true(
        self, correct_pass_criteria_request
    ):
        vpcr = ValidatedPassCriteriaRequest(correct_pass_criteria_request)
        result = vpcr.is_valid()
        assert result is True

    def test_given_pass_criteria_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_pass_criteria_request
    ):
        vpcr = ValidatedPassCriteriaRequest(incorrect_pass_criteria_request)
        result = vpcr.is_valid()
        assert result is False

    def test_given_pass_criteria_request_validator_incorrect_key_type_then_expect_false(
        self,
    ):
        vpcr = ValidatedPassCriteriaRequest(True)
        result = vpcr.is_valid()
        assert result is False

    def test_given_pass_criteria_request_validator_body_without_class_id_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_pass_criteria_request
    ):
        correct_pass_criteria_request.pop("class_id")
        vpcr = ValidatedPassCriteriaRequest(correct_pass_criteria_request)
        communicat, status = vpcr.is_valid()
        assert communicat == "No class_id was provided"
        assert status == 422

    def test_given_pass_criteria_request_validator_body_without_student_id_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_pass_criteria_request
    ):
        correct_pass_criteria_request.pop("student_id")
        vpcr = ValidatedPassCriteriaRequest(correct_pass_criteria_request)
        communicat, status = vpcr.is_valid()
        assert communicat == "No student_id was provided"
        assert status == 422

    def test_given_pass_criteria_request_validator_body_without_student_points_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_pass_criteria_request
    ):
        correct_pass_criteria_request.pop("student_points")
        vpcr = ValidatedPassCriteriaRequest(correct_pass_criteria_request)
        communicat, status = vpcr.is_valid()
        assert communicat == "No student_points was provided"
        assert status == 422
