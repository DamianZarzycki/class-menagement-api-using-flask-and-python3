import pytest

from models.class_request import ValidatedClassRequest


class TestClassRequestValidator:
    def test_given_class_request_validator_correct_body_then_expect_true(
        self, correct_class_request_body
    ):
        vcr = ValidatedClassRequest(correct_class_request_body)
        result = vcr.is_valid()
        assert result is True

    def test_given_class_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_class_request_body
    ):
        vcr = ValidatedClassRequest(incorrect_class_request_body)
        result = vcr.is_valid()
        assert result is False

    def test_given_class_request_validator_incorrect_argument_type_then_expect_false(
        self,
    ):
        vcr = ValidatedClassRequest(True)
        result = vcr.is_valid()
        assert result is False

    def test_given_class_request_validator_body_without_class_name_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("class_name")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No class_name was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_class_semester_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("class_semester")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No class_semester was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_class_year_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("class_year")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No class_year was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_isExam_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("isExam")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No isExam was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_egzaminMaxPkt_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("egzaminMaxPkt")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No egzaminMaxPkt was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_egzaminMinPkt_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("egzaminMinPkt")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No egzaminMinPkt was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_isProject_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("isProject")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No isProject was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_projektMaxPkt_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("projektMaxPkt")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No projektMaxPkt was provided"
        assert status == 422

    def test_given_class_request_validator_body_without_projektMinPkt_key_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_class_request_body
    ):
        correct_class_request_body.pop("projektMinPkt")
        vcr = ValidatedClassRequest(correct_class_request_body)
        communicat, status = vcr.is_valid()
        assert communicat == "No projektMinPkt was provided"
        assert status == 422

    def test_given_correct_value_of_class_semester_when_validating_then_expect_true(
        self, correct_class_request_body
    ):
        vcr = ValidatedClassRequest(correct_class_request_body)
        result = vcr.check_semestr_value("Letni")
        assert result == True

    def test_given_incorrect_value_of_class_semester_when_validating_then_expect_false(
        self, correct_class_request_body
    ):
        vcr = ValidatedClassRequest(correct_class_request_body)
        result = vcr.check_semestr_value(True)
        assert result == False
