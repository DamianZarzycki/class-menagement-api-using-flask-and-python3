import pytest

from models.student_request import ValidatedStudentRequest


class TestStudentRequestValidator:
    def test_given_student_request_validator_correct_body_then_expect_true(
        self, correct_student_request_body
    ):
        vsr = ValidatedStudentRequest(correct_student_request_body)
        result = vsr.is_valid()
        assert result is True

    def test_given_student_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_student_request_body
    ):
        vsr = ValidatedStudentRequest(incorrect_student_request_body)
        result = vsr.is_valid()
        assert result is False

    def test_given_student_request_validator_incorrect_argument_type_then_expect_false(
        self,
    ):
        vsr = ValidatedStudentRequest(True)
        result = vsr.is_valid()
        assert result is False

    def test_given_body_without_class_id_key_when_validating_student_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_student_request_body
    ):
        correct_student_request_body.pop("class_id")
        vsr = ValidatedStudentRequest(correct_student_request_body)
        communicat, status = vsr.is_valid()
        assert communicat == "No class_id was provided"
        assert status == 422

    def test_given_body_without_email_key_when_validating_student_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_student_request_body
    ):
        correct_student_request_body.pop("email")
        vsr = ValidatedStudentRequest(correct_student_request_body)
        communicat, status = vsr.is_valid()
        assert communicat == "No email was provided"
        assert status == 422

    def test_given_body_without_imie_key_when_validating_student_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_student_request_body
    ):
        correct_student_request_body.pop("imie")
        vsr = ValidatedStudentRequest(correct_student_request_body)
        communicat, status = vsr.is_valid()
        assert communicat == "No imie was provided"
        assert status == 422

    def test_given_body_without_nazwisko_key_when_validating_student_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_student_request_body
    ):
        correct_student_request_body.pop("nazwisko")
        vsr = ValidatedStudentRequest(correct_student_request_body)
        communicat, status = vsr.is_valid()
        assert communicat == "No nazwisko was provided"
        assert status == 422


class TestStudentPatchRequest:
    def test_given_student_patch_request_validator_correct_key_type_then_expect_true(
        self, correct_patch_student_request_body
    ):
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        result = vsr.is_valid_patch_request()
        assert result is True

    def test_given_student_patch_request_validator_incorrect_key_type_then_expect_false(
        self, incorrect_patch_student_request_body
    ):
        vsr = ValidatedStudentRequest(incorrect_patch_student_request_body)
        result = vsr.is_valid_patch_request()
        assert result is False

    def test_given_body_without_class_id_key_when_validating_student_patch_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_patch_student_request_body
    ):
        correct_patch_student_request_body.pop("class_id")
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        communicat, status = vsr.is_valid_patch_request()
        assert communicat == "No class_id was provided"
        assert status == 422

    def test_given_body_without_email_key_when_validating_student_patch_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_patch_student_request_body
    ):
        correct_patch_student_request_body.pop("email")
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        communicat, status = vsr.is_valid_patch_request()
        assert communicat == "No email was provided"
        assert status == 422

    def test_given_body_without_name_key_when_validating_student_patch_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_patch_student_request_body
    ):
        correct_patch_student_request_body.pop("name")
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        communicat, status = vsr.is_valid_patch_request()
        assert communicat == "No name was provided"
        assert status == 422

    def test_given_body_without_lastName_key_when_validating_student_patch_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_patch_student_request_body
    ):
        correct_patch_student_request_body.pop("lastName")
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        communicat, status = vsr.is_valid_patch_request()
        assert communicat == "No lastName was provided"
        assert status == 422

    def test_given_body_without_student_id_key_when_validating_student_patch_request_then_expect_422_status_and_communicat_about_missing_key(
        self, correct_patch_student_request_body
    ):
        correct_patch_student_request_body.pop("student_id")
        vsr = ValidatedStudentRequest(correct_patch_student_request_body)
        communicat, status = vsr.is_valid_patch_request()
        assert communicat == "No student_id was provided"
        assert status == 422
