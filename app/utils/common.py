import json


def decode_and_load_to_json(data):
    """
    :param data: bytecode
    :return: dict
    """
    decoded = data.decode("utf-8")
    loaded_json = json.loads(decoded)
    return loaded_json
