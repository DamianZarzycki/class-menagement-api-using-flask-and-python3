from flask import Flask
from flask_cors import CORS
from google.cloud import firestore

from views.class_view import class_views
from views.student_view import students_views
from views.pass_criteria_view import pass_criteria_views
from views.exercise_view import exercise_views


db = firestore.Client()

app = Flask(__name__)

app.app_context().push()

CORS(app)

app.register_blueprint(class_views)
app.register_blueprint(students_views)
app.register_blueprint(pass_criteria_views)
app.register_blueprint(exercise_views)

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8080, debug=True)
