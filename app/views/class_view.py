from flask import Flask, request, jsonify, make_response, redirect, session, Blueprint


from models.class_request import ValidatedClassRequest
from business_logic.class_business_logic import ClassLogic
from service.request_validation import RequestValidation


class_views = Blueprint("class", __name__)


@class_views.route("/api/classes", methods=["GET"])
def get_all_classes():
    """
    :return: tuple(string, number)
    """

    cl = ClassLogic()
    result, status = cl.get_classes()
    return result, status


@class_views.route("/api/classes", methods=["POST"])
def add_class():
    """
    :return: tuple(string, number)
    """
    data = request.json
    vcr = ValidatedClassRequest(data)
    if vcr.is_valid():
        cl = ClassLogic()
        # result = cl.add_class(vcr.body)
        cl.add_class(vcr.body)
        return {'message': 'Success'}, 201
    else:
        if not vcr.is_valid():
            return "Check types of parameters", 422
        result, status = vcr.is_valid()
        return result, status


@class_views.route("/api/classes/<class_id>", methods=["GET"])
def get_class(class_id=None):
    rv = RequestValidation()
    """
    :return: tuple(dict, number)
    """
    is_class_id_valid, is_class_id_valid_communicat = rv.check_id_parameter_for_request(
        class_id
    )
    if is_class_id_valid == True:
        cl = ClassLogic()
        result, status = cl.get_class(class_id)
        return result, status
    else:
        return "Wrong class_id type", 422


@class_views.route("/api/classes/<class_id>", methods=["DELETE"])
def delete_class(class_id=None):
    """
    :return: tuple(string, number)
    """
    rv = RequestValidation()
    provided_class_id = class_id
    # if class_id is None and student_id is None:
    if provided_class_id is None:
        return "Please provide class_id", 400

    (
        is_class_id_valid,
        is_class_id_valid_message,
    ) = rv.check_id_parameter_for_request(class_id)

    if is_class_id_valid:
        cl = ClassLogic()
        result, status = cl.delete_class(class_id)
        return result, status
    else:
        return "Wrong class_id type", 422
