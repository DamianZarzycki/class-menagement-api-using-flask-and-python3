from flask import Flask, request, jsonify, make_response, redirect, session, Blueprint

from models.pass_criteria_request import ValidatedPassCriteriaRequest
from business_logic.pass_criteria_logic import PassCriteriaLogic

pass_criteria_views = Blueprint("pass_criteria", __name__)


@pass_criteria_views.route("/api/classes/<class_id>/students/<student_id>/project", methods=["POST"])
def add_student_project_mark(class_id=None, student_id=None):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    data['student_id'] = student_id
    vpcr = ValidatedPassCriteriaRequest(data)
    is_valid = vpcr.is_valid()
    if is_valid == True:
        pcl = PassCriteriaLogic()
        result, status = pcl.mark_student_project(vpcr.body)
        return result, status
    else:
        return "Check types of parameters", 422



@pass_criteria_views.route("/api/classes/<class_id>/students/<student_id>/exam", methods=["POST"])
def add_student_exam_mark(class_id=None, student_id=None):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    data['student_id'] = student_id
    vpcr = ValidatedPassCriteriaRequest(data)
    is_valid = vpcr.is_valid()
    if is_valid == True:
        pcl = PassCriteriaLogic()
        result, status = pcl.mark_student_exam(vpcr.body)
        return result, status
    else:
        return "Check types of parameters", 422

