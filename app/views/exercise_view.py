from flask import Flask, request, jsonify, make_response, redirect, session, Blueprint

from models.exercise_request import (
    ValidatedExerciseRequest,
    ValidatedExerciseForClassRequest,
)

from business_logic.exercise_business_logic import ExerciseBusinessLogic
from service.request_validation import RequestValidation

exercise_views = Blueprint("exercise", __name__)


@exercise_views.route("/api/classes/<class_id>/exercise", methods=["GET"])
def get_exercise_from_class(class_id):
    """
    :return: tuple(list, number)
    """
    rv = RequestValidation()
    provided_class_id = class_id
    if provided_class_id is None:
        return "Please provide class_id parameter", 400

    is_class_id_valid, communicat = rv.check_id_parameter_for_request(provided_class_id)
    if is_class_id_valid:
        ebl = ExerciseBusinessLogic()
        result, status = ebl.get_exercise_from_class(provided_class_id)
        return result, status
    else:
        return "Wrong class_id type", 422


# Dodaje ćwiczenia studentowi konkretnej klasy wraz z punktacja
@exercise_views.route("/api/classes/<class_id>/students/<student_id>/exercise", methods=["PATCH"])
def add_exercise_for_student(class_id, student_id):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    data['student_id'] = student_id
    ver = ValidatedExerciseRequest(data)
    is_valid = ver.is_valid()
    # nie moge dac bez '==True' gdyż albo jest to true albo tuple wiec bez przyrownania zawsze bedzie ten warunek true
    if is_valid == True:
        ebl = ExerciseBusinessLogic()
        result, status = ebl.add_exercise_for_student(ver.body)
        return result, status
    elif not is_valid:
        return "Check types of parameters", 422
    else:
        result, status = is_valid
        return result, status


@exercise_views.route("/api/classes/<class_id>/exercise", methods=["POST"])
def add_exercise_to_class(class_id=None):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    vefcr = ValidatedExerciseForClassRequest(data)
    is_valid = vefcr.is_valid()
    if is_valid:
        ebl = ExerciseBusinessLogic()
        result, status = ebl.add_exercise_for_class(vefcr.body)

        return result, status
    else:
        return "Check types of parameters", 422

