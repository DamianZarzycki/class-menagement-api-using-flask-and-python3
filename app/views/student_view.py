from flask import Flask, request, jsonify, make_response, redirect, session, Blueprint

from models.student_request import ValidatedStudentRequest
from business_logic.student_business_logic import StudentLogic
from service.request_validation import RequestValidation

students_views = Blueprint("students", __name__)


@students_views.route("/api/classes/<class_id>/students/<student_id>", methods=["DELETE"])
def delete_specyfic_student(class_id=None, student_id=None):
    """
    :return: tuple(string, number)
    """
    rv = RequestValidation()
    provided_class_id = class_id
    provided_student_id = student_id

    if provided_class_id is None and provided_student_id is None:
        return "Please provide class_id and student_id parameters", 400
    elif provided_class_id is None:
        return "Please provide class_id parameter", 400
    elif provided_student_id is None:
        return "Please provide student_id parameter", 400

    (
        is_class_id_valid,
        is_class_id_valid_communicate,
    ) = rv.check_id_parameter_for_request(provided_class_id)
    (
        is_student_id_valid,
        is_student_id_valid_communicate,
    ) = rv.check_id_parameter_for_request(provided_student_id)

    if is_class_id_valid and is_student_id_valid:
        sl = StudentLogic()
        result, status = sl.delete_student_from_class(provided_student_id, provided_class_id)
        return result, status
    else:
        return "Wrong class_id or student_id type", 422


@students_views.route("/api/classes/<class_id>/students", methods=["POST"])
def add_student(class_id):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    vsr = ValidatedStudentRequest(data)
    is_valid = vsr.is_valid()
    if is_valid == True:
        sl = StudentLogic()
        result, status = sl.add_student_to_class(vsr.body)

        return result, status
    else:
        if is_valid == False:
            return "Check types of parameters", 422
        result, status = vsr.is_valid()
        return result, status


@students_views.route("/api/classes/<class_id>/students/<student_id>", methods=["GET"])
def get_specyfic_student(class_id=None, student_id=None):
    """
    :return: tuple(dict, number)
    """
    rv = RequestValidation()
    provided_class_id = class_id
    provided_student_id = student_id

    if provided_class_id is None and provided_student_id is None:
        return "Please provide class_id and student_id parameters", 400
    elif provided_class_id is None:
        return "Please provide class_id parameter", 400
    elif provided_student_id is None:
        return "Please provide student_id parameter", 400

    (
        is_class_id_valid,
        is_class_id_valid_communicate,
    ) = rv.check_id_parameter_for_request(provided_class_id)
    (
        is_student_id_valid,
        is_student_id_valid_communicate,
    ) = rv.check_id_parameter_for_request(provided_student_id)

    if is_class_id_valid and is_student_id_valid:
        sl = StudentLogic()
        result, status = sl.get_specyfic_student_from_class(student_id, class_id)
        return result, status
    else:
        return "Wrong class_id or student_id type", 422


@students_views.route("/api/classes/<class_id>/students", methods=["GET"])
def get_class_student(class_id):
    """
    :return: tuple(list, number)
    """
    rv = RequestValidation()
    provided_class_id = class_id
    (
        is_class_id_valid,
        is_class_id_valid_communicate,
    ) = rv.check_id_parameter_for_request(provided_class_id)

    if is_class_id_valid:
        sl = StudentLogic()
        result, status = sl.get_students_from_class(provided_class_id)
        return result, status
    else:
        return "Wrong class_id type", 422


@students_views.route("/api/classes/<class_id>/students/<student_id>", methods=["PATCH"])
def updateSpecyficStudent(class_id=None, student_id=None):
    """
    :return: tuple(string, number)
    """
    data = request.json
    data['class_id'] = class_id
    data['student_id'] = student_id
    vsr = ValidatedStudentRequest(data)
    if vsr.is_valid_patch_request() == True:
        sl = StudentLogic()
        result, status = sl.update_student(vsr.body)

        return result, status
    else:
        if vsr.is_valid_patch_request() == False:
            return {'message': 'Check types of parameters'}, 422
        result, status = vsr.is_valid_patch_request()
        return result, status
